﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterColliderMask : MonoBehaviour {

	List<GameObject> rooms;
	List<GameObject> colliders;

	float top;
	float bottom;
	float left;
	float right;

	public void Execute() {
		if (rooms == null)
			rooms = new List<GameObject> ();
		if (colliders == null)
			colliders = new List<GameObject> ();
	
		rooms.Clear ();
		DestroyAll (colliders);
		GetRooms ();
		BuildColliders ();

		top = transform.position.y + 0.5f * GetHeight (gameObject);
		bottom = transform.position.y - 0.5f * GetHeight (gameObject);
		right = transform.position.x + 0.5f * GetWidth (gameObject);
		left = transform.position.x - 0.5f * GetWidth (gameObject);
	}


	void DestroyAll(List<GameObject> l) {
		foreach (GameObject g in l) {
			DestroyImmediate (g);
		}
		l.Clear ();
	}

	void GetRooms() {
		foreach (GameObject g in GameObject.FindGameObjectsWithTag("Room")) {
			rooms.Add (g);
		}
		rooms.Sort (delegate(GameObject x, GameObject y) {
			return  (int)(x.transform.position.x - y.transform.position.x);
		});

	}

	void BuildColliders() {
		Vector3 start = transform.position;
		start.x -= 0.5f * GetWidth (gameObject);
		start.z = 0f;
		Vector3 end = start;

		foreach (GameObject g in rooms) {
			float height = GetHeight (g);
			float width = GetWidth (g);

			end = g.transform.position;
			end.x -= 0.5f * width;
			end.y = start.y;
			end.z = 0f;

			float dist = end.x - start.x;

			InstantiateWaterMask ((start + end) / 2f, new Vector2 (dist, GetHeight (gameObject)));

			float topRoom = g.transform.position.y + 0.5f * height;
			float yDist = top - topRoom;

			InstantiateWaterMask (new Vector3 (g.transform.position.x, (top + topRoom) / 2), new Vector2 (width, yDist));

			float bottomRoom = g.transform.position.y - 0.5f * height;
			yDist = bottomRoom - bottom;

			InstantiateWaterMask (new Vector3 (g.transform.position.x, (bottom + bottomRoom) / 2), new Vector2 (width, yDist));

			start = g.transform.position;
			start.x += 0.5f * width;
			start.y = transform.position.y;
			start.z = 0f;
		}

		end.x = right;
		end.y = transform.position.y;
		end.z = transform.position.z;

		InstantiateWaterMask ((start + end) / 2f, new Vector2 (end.x - start.x, GetHeight (gameObject)));
	}

	void InstantiateWaterMask (Vector3 position, Vector2 size) {
		GameObject trigger = new GameObject ();
		trigger.transform.position = position;
		trigger.AddComponent<BoxCollider2D> ();
		trigger.GetComponent<BoxCollider2D> ().size = size;
		trigger.GetComponent<BoxCollider2D> ().isTrigger = true;
		trigger.GetComponent<BoxCollider2D> ().usedByComposite = true;

		trigger.layer = 0;

		trigger.transform.parent = transform;
		trigger.name = "Trigger";
		colliders.Add (trigger);
	}

	float GetHeight(GameObject g) {
		SpriteRenderer sr = g.GetComponent<SpriteRenderer> ();
		return 2f * sr.sprite.bounds.extents.y * g.transform.localScale.y;
	}

	float GetWidth(GameObject g) {
		SpriteRenderer sr = g.GetComponent<SpriteRenderer> ();
		return 2f * sr.sprite.bounds.extents.x * g.transform.localScale.x;
	}
}
