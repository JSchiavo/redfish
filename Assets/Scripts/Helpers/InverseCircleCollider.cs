﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class InverseCircleCollider : MonoBehaviour {

    public float radius = 1f;
    public Vector2 offset = Vector2.zero;
    public float xScale = 1f;
    public float yScale = 1f;
    [Range(3, 100)]
    public int numOfPoints = 10;
    private EdgeCollider2D edge;
    private Vector2[] points;   

    #if UNITY_EDITOR
    private void Reset()
    {
        if (edge == null)
            edge = gameObject.AddComponent<EdgeCollider2D>();
        CreatePoints();
        edge.points = points;
    }
    
    private void Update()
    {
        CreatePoints();
        ScalePointsByDimension();
        ScalePointsByRadius();
        ApplyOffset();
        edge.points = points;
    }
    #endif

    void CreatePoints()
    {
        points = new Vector2[numOfPoints];
        for (int i = 0; i < numOfPoints; i++)
        {
            float x = ((float)i / (float)(numOfPoints - 1)) * 2 * Mathf.PI;
            float y = ((float)i / (float)(numOfPoints - 1)) * 2 * Mathf.PI;
            points[i] = new Vector2(Mathf.Cos(x), Mathf.Sin(y));
        }
    }

    void ScalePointsByDimension()
    {
        for (int i = 0; i < numOfPoints; i++)
        {
            points[i] = new Vector2(points[i].x * xScale, points[i].y * yScale);
        }
    }

    void ScalePointsByRadius()
    {
        for (int i = 0; i < numOfPoints; i++)
        {
            points[i] = radius * points[i];
        }
    }

    void ApplyOffset()
    {
        for (int i = 0; i < numOfPoints; i++)
        {
            points[i] = points[i] + offset;
        }
    }

    private void OnDisable()
    {
        DestroyImmediate(edge);
    }
}
