﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCard : GameElement {

    public Player player;

    public Image healthBar;
    private Image playerImage;
    private Image specialImage;

	public void Setup(Player p)
    {
        player = p;
    }

    private void Update()
    {
        healthBar.fillAmount = player.GetState().health / 100f;
    }
}
