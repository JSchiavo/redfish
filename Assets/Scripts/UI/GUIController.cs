﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class GUIController : GameElement {

    public PlayerCard[] playerCards;

    void Awake()
    {
        if (playerCards.Length < 4)
        {
            Debug.LogWarning("GUIManager: Missing Player Card placeholder(s)");
            return;
        }
    }

    void OnGUISetup (Player p)
    {
        SetupHealthController(p);
    }

    void SetupHealthController(Player p)
    {
        switch (p.ctrl)
        {
            case XboxController.First:
                playerCards[0].gameObject.SetActive(true);
                playerCards[0].Setup(p);
                break;
            case XboxController.Second:
                playerCards[1].gameObject.SetActive(true);
                playerCards[1].Setup(p);
                break;
            case XboxController.Third:
                playerCards[2].gameObject.SetActive(true);
                playerCards[2].Setup(p);
                break;
            case XboxController.Fourth:
                playerCards[3].gameObject.SetActive(true);
                playerCards[3].Setup(p);
                break;
        }
    }
}
