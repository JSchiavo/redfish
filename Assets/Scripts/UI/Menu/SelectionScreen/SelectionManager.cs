﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class SelectionManager : MonoBehaviour {

    public PointerPlaceholder pp;
    public GameObject playButton;
    public Character[] characters;

    private int currentNumOfPlayers = 0;
	
	// Update is called once per frame
	void Update () {
        CheckForPlayers();
        HandleInput();
        UpdateScreen();
	}

    void UpdateScreen()
    {
        playButton.SetActive(pp.AllPlayersHaveSelected());
    }

    void CheckForPlayers()
    {
        if (currentNumOfPlayers != XCI.GetNumPluggedCtrlrs())
        {
            currentNumOfPlayers = XCI.GetNumPluggedCtrlrs();
            pp.RefreshPointers();
        }
    }

    void HandleInput()
    {
        MovePointers();
        Select();
    }

    void MovePointers()
    {
        MovePointer(XboxController.First, GetDirection(XboxController.First));
        MovePointer(XboxController.Second, GetDirection(XboxController.Second));
        MovePointer(XboxController.Third, GetDirection(XboxController.Third));
        MovePointer(XboxController.Fourth, GetDirection(XboxController.Fourth));
    }

    void MovePointer(XboxController ctrl, int direction)
    {
        pp.MovePointer(ctrl, direction);
    }

    void Select()
    {
        pp.HandleSelection(XboxController.First);
        pp.HandleSelection(XboxController.Second);
        pp.HandleSelection(XboxController.Third);
        pp.HandleSelection(XboxController.Fourth);
    }

    public void Play()
    {
        Character[] selections = new Character[currentNumOfPlayers];
        for (int i = 0; i < currentNumOfPlayers; i++)
        {
            selections[i] = characters[pp.GetPointerPosition(i)];
        }
        GameManager.manager.SaveCharacterSelections(selections);
        GameManager.manager.LoadScene("Main");
    }

    int GetDirection(XboxController ctrl)
    {
        float dir = XCI.GetAxis(XboxAxis.LeftStickX, ctrl);
        if (dir > 0.5f)
            return 1;
        if (dir < -0.5f)
            return -1;
        else
            return 0;
    }
}
