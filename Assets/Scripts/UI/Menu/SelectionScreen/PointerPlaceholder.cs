﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using XboxCtrlrInput;

public class PointerPlaceholder : MonoBehaviour {

    public GameObject[] placeholders;
    public GameObject[] pointers;
    public float delay;

    private int[] pointerPosition = new int[4];
    private bool[] hasSelected = new bool[4];
    private bool[] inputDelay = new bool[4];

    public void MovePointer(XboxController ctrl, int direction)
    {
        if (direction == 0)
            return;

        int player = XCI.ControllerToInt(ctrl);
        if (!inputDelay[player] && !hasSelected[player])
        {
            int newPos = pointerPosition[player] + direction;
            AttachPlayerPointerToPlaceholderAt(player, newPos);
            StartCoroutine(Delay(player));
        }
    }

    void AttachPlayerPointerToPlaceholderAt(int player, int placeholderPos)
    {
        if (VerifyPosition(placeholderPos))
        {
            pointers[player].transform.SetParent (placeholders[placeholderPos].transform);
            pointerPosition[player] = placeholderPos;
        }
    }

    bool VerifyPosition(int pos)
    {
        if (pos < 0)
            return false;
        if (pos >= placeholders.Length)
            return false;
        return true;
    }

    public void HandleSelection(XboxController ctrl)
    {
        int player = XCI.ControllerToInt(ctrl);
        if (XCI.GetButtonDown(XboxButton.A, ctrl))
            hasSelected[player] = true;
        if (XCI.GetButtonDown(XboxButton.B, ctrl))
            hasSelected[player] = false;
    }

    public int GetPointerPosition(int player)
    {
        return pointerPosition[player];
    }

    public bool AllPlayersHaveSelected()
    {
        int playerCount = XCI.GetNumPluggedCtrlrs();
        for (int i = 0; i < hasSelected.Length; i++)
        {
            playerCount = hasSelected[i] ? playerCount - 1 : playerCount;
        }
        return playerCount == 0;
    }

    IEnumerator Delay(int player)
    {
        inputDelay[player] = true;
        yield return new WaitForSeconds(delay);
        inputDelay[player] = false;
    }

    public void RefreshPointers()
    {
        pointers[0].SetActive(XCI.IsPluggedIn(1));
        pointers[1].SetActive(XCI.IsPluggedIn(2));
        pointers[2].SetActive(XCI.IsPluggedIn(3));
        pointers[3].SetActive(XCI.IsPluggedIn(4));
    }

}
