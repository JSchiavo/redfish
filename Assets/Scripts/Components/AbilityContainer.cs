﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class AbilityContainer : PlayerElement {

	public Ability primary;
	public Ability ability1;
	public Ability ability2;

	private Cooldown pC;
	private Cooldown a1C;
	private Cooldown a2C;


	void Start() {
		SetCooldowns ();
	}

	void Update() {
		CheckInput ();
	}

	void SetCooldowns() {
		if (primary != null)
			pC = new Cooldown (primary.cooldown);
		if (ability1 != null)
			a1C = new Cooldown (ability1.cooldown);
		if (ability2 != null)
			a2C = new Cooldown (ability2.cooldown);
	}

	void CheckInput() {
		if (input.PrimaryHold(GetPlayer().ctrl)) {
			if (primary != null && pC.available) {
				primary.UseAbility (GetPlayer ());
				StartCoroutine (StartCooldown (pC));
			}
		}
		if (input.Ability1Press(GetPlayer().ctrl)) {
			if (ability1 != null && a1C.available) {
				ability1.UseAbility (GetPlayer ());
				StartCoroutine (StartCooldown (a1C));
			}
		}
		if (input.Ability2Press(GetPlayer().ctrl)) {
			if (ability2 != null && a2C.available) {
				ability2.UseAbility (GetPlayer ());
				StartCoroutine (StartCooldown (a2C));
			}
		}
	}

	IEnumerator StartCooldown(Cooldown c) {
		c.available = false;
		yield return new WaitForSeconds (c.cooldown);
		c.available = true;
	}

	private class Cooldown {
		public bool available;
		public float cooldown;

		public Cooldown(float cooldown) {
			this.available = true;
			this.cooldown = cooldown;
		}
	}

}
