﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class Gun : PlayerElement {

	public Transform gunBarrel;

    public void ShootProjectile(Projectile projectile)
    {
		Transform t = gunBarrel != null ? gunBarrel : transform;
        Vector2 direction = new Vector2(GetState().h + Random.Range(0f, 0.05f), GetState().v + Random.Range(0f, 0.05f));
        if (direction.x < 0f)
        {
            projectile.transform.localScale = new Vector3(-projectile.transform.localScale.x, projectile.transform.localScale.y, projectile.transform.localScale.z);

        }
        projectile.transform.position = t.position;
        projectile.transform.position = new Vector3(projectile.transform.position.x, projectile.transform.position.y, -3f);
		projectile.SetVelocity(direction * (projectile.pa.force / direction.magnitude));
        Physics2D.IgnoreCollision(projectile.GetComponent<Collider2D>(), GetComponentInParent<Collider2D>(), true);
        projectile.AdditionalSetup(GetPlayer());
    }
}
