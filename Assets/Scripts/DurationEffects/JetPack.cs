﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class JetPack : DurationEffect {

	public GameObject particlePrefab;
	GameObject particle;
	Rigidbody2D rigid;
	public float thrust;

	public override void Begin ()
	{
		rigid = target.GetComponent<Rigidbody2D> ();
		particle = Instantiate (particlePrefab);
	}

	void Update() {
		float flip = target.transform.localScale.x < 0f ? -1f : 1f;
		particle.transform.position = target.transform.position + (new Vector3 (-0.45f * flip, 0.8f, 0f));
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (target.input.JumpHold(target.ctrl)) {
			rigid.velocity += Vector2.up * thrust * Time.fixedDeltaTime;
		}
	}

	void OnDestroy() {
		Destroy (particle);
	}

	public override void End ()
	{
		return;
	}
}
