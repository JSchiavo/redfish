﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : DurationEffect {

	float originalGroundSpeed;
	float originalSwimSpeed;

	public float speedMultiplier;

	public override void Begin ()
	{
		originalGroundSpeed = target.GetStats ().groundSpeed;
		originalSwimSpeed = target.GetStats ().swimSpeed;
		target.GetStats ().groundSpeed *= speedMultiplier;
		target.GetStats ().swimSpeed *= speedMultiplier;
	}

	public override void End ()
	{
		target.GetStats ().groundSpeed = originalGroundSpeed;
		target.GetStats ().swimSpeed = originalSwimSpeed;
	}
}
