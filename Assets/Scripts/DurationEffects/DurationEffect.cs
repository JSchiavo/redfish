﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DurationEffect : MonoBehaviour {

	[HideInInspector]
	public Player target;
	[HideInInspector]
	public DurationAbility da;

	// Use this for initialization
	public void Initiate() {
		Begin ();
		StartCoroutine (Use ());
	}

	public IEnumerator Use() {
		yield return new WaitForSeconds (da.duration);
		End ();
		Destroy (gameObject);
	}

	public abstract void Begin();
	public abstract void End();
}
