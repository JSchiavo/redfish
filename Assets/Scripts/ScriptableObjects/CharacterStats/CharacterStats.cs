﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Character Stats", menuName = "CharacterStats", order = 1)]
[System.Serializable]
public class CharacterStats : ScriptableObject {
    public float swimSpeed = 1.0f;
    public float groundSpeed = 1.0f;
    public float acceleration = 0.3f;
    public float jumpForce = 1.0f;
    public float fallSpeed = 1.0f;
}
