﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
[CreateAssetMenu(fileName = "New Xbox Input Profile", menuName = "InputProfile/XboxInputProfile", order = 1)]
public class XboxInputProfile : InputProfile{

	public XboxButton Jump;
	public XboxButton Primary;
	public XboxButton Ability1;
	public XboxButton Ability2;
	public XboxButton Ability3;

	public override float x (XboxController ctrl)
	{
		return XCI.GetAxisRaw (XboxAxis.LeftStickX, ctrl);
	}
	public override float y (XboxController ctrl)
	{
		return XCI.GetAxisRaw (XboxAxis.LeftStickY, ctrl);
	}
	public override float h (XboxController ctrl, Vector3 position)
	{
		return XCI.GetAxisRaw (XboxAxis.RightStickX, ctrl);
	}
	public override float v (XboxController ctrl, Vector3 position)
	{
		return XCI.GetAxisRaw (XboxAxis.RightStickY, ctrl);
	}
	public override bool JumpPress (XboxController ctrl)
	{
		return XCI.GetButtonDown (Jump, ctrl);
	}
	public override bool PrimaryPress (XboxController ctrl)
	{
		return XCI.GetButtonDown (Primary, ctrl);
	}
	public override bool Ability1Press (XboxController ctrl)
	{
		return XCI.GetButtonDown (Ability1, ctrl);
	}
	public override bool Ability2Press (XboxController ctrl)
	{
		return XCI.GetButtonDown (Ability2, ctrl);
	}
	public override bool Ability3Press (XboxController ctrl)
	{
		return XCI.GetButtonDown (Ability3, ctrl);
	}
	public override bool JumpHold (XboxController ctrl)
	{
		return XCI.GetButton (Jump, ctrl);
	}
	public override bool PrimaryHold (XboxController ctrl)
	{
		return XCI.GetButton (Primary, ctrl);
	}
	public override bool Ability1Hold (XboxController ctrl)
	{
		return XCI.GetButton (Ability1, ctrl);
	}
	public override bool Ability2Hold (XboxController ctrl)
	{
		return XCI.GetButton (Ability2, ctrl);
	}
	public override bool Ability3Hold (XboxController ctrl)
	{
		return XCI.GetButton (Ability3, ctrl);
	}
}
