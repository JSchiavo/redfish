﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

[CreateAssetMenu(fileName = "New Keyboard Input Profile", menuName = "InputProfile/KeyboardInputProfile", order = 1)]
public class KeyboardInputProfile : InputProfile {

	public KeyCode Left;
	public KeyCode Right;
	public KeyCode Up;
	public KeyCode Down;
	public KeyCode Jump;
	public KeyCode Primary;
	public KeyCode Ability1;
	public KeyCode Ability2;
	public KeyCode Ability3;

	public override float x (XboxController ctrl)
	{
		return (Input.GetKey (Left) ? -1f : 0f) + (Input.GetKey (Right) ? 1f : 0f);
	}
	public override float y (XboxController ctrl)
	{
		return (Input.GetKey (Down) ? -1f : 0f) + (Input.GetKey (Up) ? 1f : 0f);
	}
	public override float h (XboxController ctrl, Vector3 position)
	{
		Vector3 dir = Camera.main.ScreenToWorldPoint (Input.mousePosition) - position;
		dir.z = 0f;
		return dir.normalized.x;
	}
	public override float v (XboxController ctrl, Vector3 position)
	{
		Vector3 dir = Camera.main.ScreenToWorldPoint (Input.mousePosition) - position;
		dir.z = 0f;
		return dir.normalized.y;
	}
	public override bool JumpPress (XboxController ctrl)
	{
		return Input.GetKeyDown (Jump);
	}
	public override bool PrimaryPress (XboxController ctrl)
	{
		return Input.GetKeyDown (Primary);
	}
	public override bool Ability1Press (XboxController ctrl)
	{
		return Input.GetKeyDown (Ability1);
	}
	public override bool Ability2Press (XboxController ctrl)
	{
		return Input.GetKeyDown (Ability2);
	}
	public override bool Ability3Press (XboxController ctrl)
	{
		return Input.GetKeyDown (Ability3);
	}
	public override bool JumpHold (XboxController ctrl)
	{
		return Input.GetKey (Jump);
	}
	public override bool PrimaryHold (XboxController ctrl)
	{
		return Input.GetKey (Primary);
	}
	public override bool Ability1Hold (XboxController ctrl)
	{
		return Input.GetKey (Ability1);
	}
	public override bool Ability2Hold (XboxController ctrl)
	{
		return Input.GetKey (Ability2);
	}
	public override bool Ability3Hold (XboxController ctrl)
	{
		return Input.GetKey (Ability3);
	}
}
