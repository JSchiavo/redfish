﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public abstract class InputProfile : ScriptableObject {
	public abstract float x (XboxController ctrl);
	public abstract float y (XboxController ctrl);
	public abstract float h (XboxController ctrl, Vector3 position);
	public abstract float v (XboxController ctrl, Vector3 position);

	public abstract bool JumpPress (XboxController ctrl);
	public abstract bool PrimaryPress (XboxController ctrl);
	public abstract bool Ability1Press (XboxController ctrl);
	public abstract bool Ability2Press (XboxController ctrl);
	public abstract bool Ability3Press (XboxController ctrl);
	public abstract bool JumpHold (XboxController ctrl);
	public abstract bool PrimaryHold (XboxController ctrl);
	public abstract bool Ability1Hold (XboxController ctrl);
	public abstract bool Ability2Hold (XboxController ctrl);
	public abstract bool Ability3Hold (XboxController ctrl);
}
