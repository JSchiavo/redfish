﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Duration Ability", menuName = "Abilities/DurationAbility", order = 1)]
public class DurationAbility : Ability {

	public DurationEffect effect;
	public float duration;

	public override void UseAbility (Player p)
	{
		GameObject obj = Instantiate (effect.gameObject) as GameObject;
		obj.GetComponent<DurationEffect> ().target = p;
		obj.GetComponent<DurationEffect> ().da = this;
		obj.GetComponent<DurationEffect> ().Initiate ();
	}
}
