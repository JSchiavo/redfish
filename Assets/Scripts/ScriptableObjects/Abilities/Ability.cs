﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability : ScriptableObject {

	public float cooldown;

	public abstract void UseAbility(Player p);
}
