﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Projectile Ability", menuName = "Abilities/ProjectileAbility", order = 1)]
public class ProjectileAbility : Ability {

	public GameObject projectile;

	public float force;
	public bool destroyOnCollision = true;
	public bool randomizeTorque = true;
	public bool faceTowardsVelocity = false;
	public float timeUntilDestroy = 10f;

	public override void UseAbility (Player p)
	{
		GameObject obj = Instantiate(projectile) as GameObject;
		obj.GetComponent<Projectile> ().pa = this;
		obj.GetComponent<Projectile> ().timeUntilDestroy = this.timeUntilDestroy;
		p.GetGun ().ShootProjectile (obj.GetComponent<Projectile> ());
	}

}
