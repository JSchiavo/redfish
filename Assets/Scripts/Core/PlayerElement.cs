﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * PlayerElement should be inherited by any player specific scripts that need to subscirbe to player events.
 * PlayerElements will have access to all global managers as well as the Player container class. 
 */
public class PlayerElement : GameElement {

	protected Player GetPlayer() {
		return gameObject.GetComponentInParent<Player> ();
	}

	protected PlayerEventManager GetPlayerEventManager() {
		return GetPlayer().GetPlayerEventManager ();
	}

	protected CharacterStats GetStats() {
		return GetPlayer ().GetStats ();
	}

    protected PlayerState GetState()
    {
        return GetPlayer().GetState();
    }

	protected InputProfile input {
		get { return GetPlayer ().input; }
	}
}
