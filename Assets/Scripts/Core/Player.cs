﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

/*
 * Container class that will hold player specific models, events, etc. This class will be at the root of
 * all PlayerElements.
 */
[RequireComponent(typeof(PlayerEventManager))]
[RequireComponent(typeof(PlayerState))]
public class Player : GameElement {

    public XboxController ctrl;
	public InputProfile input;

	private Gun gun;
    private PlayerEventManager pem;
    public CharacterStats characterStats;
	private PlayerState playerState;

	// Use this for initialization
	void Start () {
		characterStats = Instantiate (characterStats) as CharacterStats;
		gun = gameObject.GetComponentInChildren<Gun> ();
		pem = gameObject.GetComponent<PlayerEventManager> ();
        playerState = gameObject.GetComponent<PlayerState>();
        if (characterStats == null)
            Debug.LogError("Player: Missing CharacterStats");
        EventManager.OnGUISetup(this);
    }

    public PlayerEventManager GetPlayerEventManager() {
		return pem;
	}

	public CharacterStats GetStats()
    {
		return characterStats;
	}

    public PlayerState GetState()
    {
        return playerState;
    }

	public Gun GetGun() {
		return gun;
	}
}
