﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Any script that will be subscribing to global events should inherit from this class.
 * GameElement will also have access to other global Managers such as ParticleManager, AssetManager,
 * Audio Manager, etc.
 */
public class GameElement : MonoBehaviour {
	
}
