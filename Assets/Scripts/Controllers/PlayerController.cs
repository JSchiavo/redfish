﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class PlayerController : PlayerElement, IDamageable
{
	
    Rigidbody2D rigid;

    // Use this for initialization
    void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateData();
        CheckInput();
    }

    void FixedUpdate()
    {
        if (!GetState().isStunned)
            ApplyVelocity();

        ApplyGravity();
    }

    void UpdateData()
    {
        PlayerState m = GetState();
        Player p = GetPlayer();
        m.velocity = rigid.velocity;
		m.position = transform.position;
		m.x = input.x(p.ctrl);
		m.y = input.y(p.ctrl);
		m.h = input.h(p.ctrl, m.position);
		m.v = input.v(p.ctrl, m.position);
    }

    void BasicShoot()
    {
		EventManager.OnBasicShoot (GetPlayer ());
    }

	void SpecialShoot()
	{
		EventManager.OnSpecialShoot (GetPlayer ());
	}

    void ApplyVelocity()
    {
        float speed = GetState().inWater ? GetStats().swimSpeed : GetStats().groundSpeed;
        Vector2 dir = new Vector2(GetState().x, GetState().y);
        Vector2 targetVelocity = dir * speed;
        Vector2 force = (targetVelocity - rigid.velocity) * GetStats().acceleration;
		if (GetState ().inWater)
			rigid.velocity += force * Time.fixedDeltaTime;
        else
			rigid.velocity += new Vector2 (force.x, 0.0f) * Time.fixedDeltaTime;
    }

    void CheckInput()
    {
		if (input.JumpPress(GetPlayer().ctrl) && GetState().canDblJump && !GetState().isStunned)
        {
            GetState().canDblJump = GetState().isGrounded;
			rigid.velocity += Vector2.up * GetStats ().jumpForce;
        }
    }

    void ApplyGravity()
    {
		if (rigid.velocity.y > 0.0f && input.JumpHold(GetPlayer().ctrl))
            return;
		if (!GetState ().inWater)
			rigid.velocity += Vector2.up * Physics2D.gravity.y * Time.fixedDeltaTime * GetStats ().fallSpeed * Time.fixedDeltaTime;
    }

    IEnumerator Recovery()
    {
        GetState().isStunned = true;
        while (GetState().health < 100f)
        {
            GetState().health += 5f;
            yield return new WaitForSeconds(0.25f);
        }
        GetPlayerEventManager().OnPlayerRecovered();
        GetState().isStunned = false;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.layer == 0)
            GetState().inWater = true;
    }

    void OnTriggerExit2D(Collider2D c)
    {
        if (c.gameObject.layer == 0)
            GetState().inWater = false;
    }

    void OnCollisionEnter2D(Collision2D c)
    {
		if (c.gameObject.layer != 8) {
			if (c.contacts [0].normal.x > -0.2f && c.contacts [0].normal.x < 0.2f)
				GetPlayerEventManager ().OnGroundEnter ();
		}

    }

    void OnCollisionExit2D(Collision2D c)
    {
        GetPlayerEventManager().OnGroundExit();
    }

    void OnGroundEnter()
    {
        GetState().canDblJump = true;
        GetState().isGrounded = true;
    }

    void OnGroundExit()
    {
        GetState().isGrounded = false;
    }

    void OnPlayerStunned()
    {
        rigid.constraints = RigidbodyConstraints2D.None;
        StartCoroutine(Recovery());
        if (!GetState().inWater)
            rigid.AddForce(Vector2.up * 2f, ForceMode2D.Impulse);  
    }

    void OnPlayerRecovered()
    {
        transform.rotation = Quaternion.identity;
        rigid.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

	public void Damage(Damage damage) {
        float d = 0f;
        if (damage.type == DamageType.Area)
        {
            rigid.AddForce(damage.scaledDamage * damage.direction, ForceMode2D.Impulse);
            d = damage.scaledDamage;
        }
        else
            d = damage.damage;

        if (!GetState().isStunned)
            GetState().health -= d;
	}
}
