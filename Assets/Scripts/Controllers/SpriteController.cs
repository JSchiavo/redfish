﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SpriteController: PlayerElement
{
    public GameObject Head;
    public GameObject Arm;
    public GameObject Bone1;
    public GameObject Bone2;

    public bool useIK;
	[SerializeField]
    public float speed;

    public float armOffset;
    public float min;
    public float max;

    private Vector2 lastDirection;
    // Update is called once per frame
    void LateUpdate()
    {

        Vector2 direction = new Vector2(GetState().h, GetState().v);

        if (GetState().rMag < 0.2f)
            direction = lastDirection.x > 0f ? Vector2.right : Vector2.left;

        float angle;
        float flip = direction.x > 0f ? 1f : -1f;

        Vector3 myScale = transform.localScale;
        myScale.x = flip * Mathf.Abs(myScale.x);
        transform.localScale = myScale;

        angle = Mathf.Atan2(direction.y, flip * direction.x) * Mathf.Rad2Deg;
        angle = Mathf.Clamp(angle, min, max);
        
        Head.transform.localRotation = Quaternion.Lerp(Head.transform.localRotation, Quaternion.AngleAxis(angle, Vector3.forward), Time.deltaTime * speed);

		if (Arm != null && !useIK)
			Arm.transform.localRotation = Quaternion.Lerp (Arm.transform.localRotation, Quaternion.AngleAxis (angle + armOffset, Vector3.forward), Time.deltaTime * speed);
        else
            SolveIK(flip);
        lastDirection = direction;
    }

    void SolveIK(float flip)
    {
        if (Bone1 == null || Bone2 == null)
        {
            Debug.LogError("SpriteController: Missing a bone reference for IK");
            return;
        }

        float a1 = Bone1.GetComponent<SpriteRenderer>().sprite.bounds.size.y;
        float a2 = Bone2.GetComponent<SpriteRenderer>().sprite.bounds.size.y;

        Vector2 target = new Vector2(flip * GetState().h * (a1 + a2), GetState().v * (a1 + a2));

        float d1 = Mathf.Pow(target.x, 2f) + Mathf.Pow(target.y, 2f);
        float d2 = Mathf.Pow(a1, 2f) + Mathf.Pow(a2, 2f);

        float t1 = -90f + armOffset;
        float t2 = 90f;

        if (d1 != 0f && GetState().rMag >= 0.2f)
        {
            float ct2 = -(d1 - Mathf.Pow(a1, 2f) + Mathf.Pow(a2, 2f)) / (2 * a1 * a2);
            ct2 = Mathf.Clamp(ct2, -1f, 1f);

            t2 = 180f - Mathf.Acos(ct2) * Mathf.Rad2Deg;

            float alpha = Mathf.Atan(((a2 * Mathf.Sin(t2 * Mathf.Deg2Rad)) / (a1 + a2 * Mathf.Cos(t2 * Mathf.Deg2Rad))));
            float beta = Mathf.Atan((target.y / (target.x + 0.01f)));
            t1 = beta - alpha;
            t1 = t1 * Mathf.Rad2Deg + armOffset;
        }
        
        Bone1.transform.localRotation = Quaternion.Lerp(Bone1.transform.localRotation, Quaternion.AngleAxis(t1, Vector3.forward), Time.deltaTime * speed);
        Bone2.transform.localRotation = Quaternion.Lerp(Bone2.transform.localRotation, Quaternion.AngleAxis(t2, Vector3.forward), Time.deltaTime * speed);
    }
}
