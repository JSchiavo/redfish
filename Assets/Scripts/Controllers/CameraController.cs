﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : GameElement {

    public List<Transform> targets;
    public Vector3 offset;
    public float positionSmooth = 1;
    public float zoomSmooth = 1;
    private Vector3 velocity;
    private Camera cam;


    public float padding;
    private float width;
    private float height;

	void Awake () {
		cam = gameObject.GetComponent<Camera> ();
        targets = new List<Transform>();
        Setup();
	}

    // TODO: Come up with better way to add players to camera
    void Setup()
    {
        foreach (PlayerController p in GameObject.FindObjectsOfType<PlayerController>())
        {
            AddTarget(p.gameObject.transform);
        }
    }
	
	void LateUpdate () {
        if (targets.Count > 0)
        {
            transform.position = position;
            cam.orthographicSize = zoom;
        }
        UpdateDimensions();
	}

    public void AddTarget(Transform target)
    {
        targets.Add(target);
    }

    private void UpdateDimensions()
    {
        height = (2f * (cam.orthographicSize + padding));
        width = (height * cam.aspect);
    }

    private Bounds CalculateBounds()
    {
        Bounds bounds = new Bounds();
        bounds.min = Vector3.one;
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }
        return bounds;
    }

    private Vector3 position
    {
        get
        {
            Bounds bounds = CalculateBounds();
            Vector3 center = bounds.center;
            center += offset;
            center = Vector3.SmoothDamp(transform.position, center, ref velocity, positionSmooth);
            return center;
        }
    }

    private float zoom
    {
        get
        {
            Bounds bounds = CalculateBounds();
            float targetZoom = cam.orthographicSize;
            if (bounds.size.x != width)
                targetZoom += bounds.size.x - width;
            if (bounds.size.y != height)
                targetZoom += bounds.size.y - height;
            return Mathf.Lerp(cam.orthographicSize, targetZoom + 10f, Time.deltaTime * zoomSmooth);
        }
    }
}
