﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimationController : PlayerElement {

	Animator anim;

	bool walking {
		set {
			anim.SetBool ("Walking", value);
		}
		get {
			return anim.GetBool ("Walking");
		}
	}

	bool backwards {
		set {
			anim.SetBool ("Backward", value);
		}
		get {
			return anim.GetBool ("Backward");
		}
	}
		
	void Start () {
		anim = GetComponent<Animator> ();
	}
		
	void Update () {
		Walk ();
	}

	void Walk() {
		PlayerState ps = GetState ();

		walking = (Mathf.Abs (ps.velocity.x) > 0.5f);

		bool facingRight = transform.localScale.x >= 0f;
		bool movingBackward = facingRight ? ps.velocity.x < 0f : ps.velocity.x > 0f;

		backwards = movingBackward;

	}
}
