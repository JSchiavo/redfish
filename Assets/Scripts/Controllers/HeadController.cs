﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadController :MonoBehaviour {
    public GameObject Head;
	// Use this for initialization
	void Start () {
		
	}
    public float moveSpeed = 5f;
	// Update is called once per frame
	void Update () {
        
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        if (direction.x > 0)
        {
        }
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        float angleClamped = Mathf.Clamp(angle, -50f, 40f);
        Quaternion rotation = Quaternion.AngleAxis(angleClamped, Vector3.forward);
        transform.localRotation = rotation;


        // testing
        if (Input.GetKey(KeyCode.P))
        {
            Debug.Log(direction);
        }
    }
}
