﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PufferfishGrenadeProjectile : Projectile {

	public float explodeAfterTimer = 5.0f;
	public float explodeAfterCollisionTimer = 1.0f;
	public float explosionRadius = 5.0f;
    public float shrapnelForce;
	public int shrapnelCount;
	public GameObject shrapnel;

	private bool hasExploded = false;
	private bool isGrounded = false;


    void Update() {
		if ((explodeAfterTimer <= 0f || explodeAfterCollisionTimer <= 0f) && !hasExploded) {
			hasExploded = true;
			Detonate ();
			Destroy (gameObject);
		}
		
		if (hasCollided)
			explodeAfterCollisionTimer -= Time.deltaTime;
		else
			explodeAfterTimer -= Time.deltaTime;
	}

	void OnCollisionEnter2D(Collision2D c) {
		hasCollided = true;
		isGrounded = true;
	}

	void OnCollisionExit2D(Collision2D c) {
		isGrounded = false;
	}

	void Detonate() {
        CreateShrapnel();
        ApplyDamage();
	}

    void CreateShrapnel()
    {
        float mult = isGrounded ? Mathf.PI : 2 * Mathf.PI;
        for (int i = 0; i < shrapnelCount; i++)
        {
            GameObject s = (GameObject)Instantiate(shrapnel);
            Vector3 pos = transform.position;
            float x = Mathf.Cos(((float)i / (float)shrapnelCount) * mult);
            float y = Mathf.Sin(((float)i / (float)shrapnelCount) * mult);
            pos.x += x; pos.y += y;
            s.transform.position = pos;
            if (CheckForOverlap(s))
            {
                Destroy(s);
                continue;
            }

            Vector2 direction = new Vector2(x + Random.Range(0f, 0.5f), y + Random.Range(0f, 0.5f));
            s.GetComponent<Rigidbody2D>().velocity = (direction * shrapnelForce);
        }
    }

    bool CheckForOverlap(GameObject g)
    {
        LayerMask mask = new LayerMask();
        mask.value = LayerMask.GetMask("Player");
        ContactFilter2D filter = new ContactFilter2D();
        filter.SetLayerMask(mask);
        return g.GetComponent<Rigidbody2D>().OverlapCollider(filter, new Collider2D[1]) > 0;
    }

    void ApplyDamage()
    {
        Damage d = GetComponent<Damage>();
        foreach (Collider2D c in Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), explosionRadius))
        {
            if (c.GetComponent<IDamageable>() != null)
            {
                Vector2 dir = c.gameObject.transform.position - transform.position;
                float distance = dir.magnitude;
                float tempDamage = (explosionRadius - distance) / explosionRadius;
                tempDamage *= d.damage;
                d.scaledDamage = tempDamage;
                d.direction = dir.normalized;
                c.gameObject.GetComponent<IDamageable>().Damage(d);
            }
        }
    }

}
