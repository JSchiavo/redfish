﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (Rigidbody2D))]
[RequireComponent(typeof (BoxCollider2D))]
[RequireComponent(typeof (Damage))]
public class Projectile : MonoBehaviour {

	[HideInInspector]
	public ProjectileAbility pa;
	[HideInInspector]
	public float timeUntilDestroy;

    private Rigidbody2D rigid;

    protected bool hasCollided = false;
    internal float lifetime = 0f;
    internal Vector3 velocity;

	void Start() {
		rigid = gameObject.GetComponent<Rigidbody2D> ();
        if (pa.randomizeTorque)
		    rigid.AddTorque(Random.Range(10f, 270f));
	}

	void Update() {
        OnUpdate();
	}

    protected virtual void OnUpdate()
    {
        UpdateData();
        if (pa.faceTowardsVelocity && !hasCollided)
            transform.localRotation = Quaternion.AngleAxis(Mathf.Atan(velocity.y / velocity.x) * Mathf.Rad2Deg, Vector3.forward);
        CheckIfValid();
    }

    protected virtual void OnCollision(Collision2D c)
    {
        if (c.gameObject.GetComponentInChildren<IDamageable>() != null && !hasCollided)
            c.gameObject.GetComponentInChildren<IDamageable>().Damage(GetComponent<Damage>());
        if (pa.destroyOnCollision)
            Destroy(gameObject);
        hasCollided = true;
    }

	void UpdateData() {
		lifetime += Time.deltaTime;
		timeUntilDestroy -= Time.deltaTime;
		velocity = rigid.velocity;
	}

	void CheckIfValid() {
		if (timeUntilDestroy < 0f)
			Destroy (gameObject);
	}

	public void SetVelocity(Vector3 v) {
		GetComponent<Rigidbody2D> ().velocity = v;
	}

    public virtual void AdditionalSetup(Player p)
    {
        return;
    }

	void OnCollisionEnter2D(Collision2D c) {
        OnCollision(c);
    }
}
