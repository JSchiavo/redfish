﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeHarpoon : Projectile {

    public Material ropeMaterial;

    Player p;
    LineRenderer rope;
    List<Node> nodes;
    Node lastNode;
    Transform target;
	SpringJoint2D targetJoint;
    Vector3 targetOffset;

    public float segmentLength;
	public int _maxLength = 10;
	public int maxLength {
		get { return _maxLength; }
		set {
			if (value == 0) {
				Stop ();
			}
			_maxLength = value;
		}
	}
	
	// Update is called once per frame
	protected override void OnUpdate () {
        base.OnUpdate();

		if (!hasCollided && maxLength > 0)
            UpdateRope();

		if (target != null) {
			transform.position = target.position + targetOffset;
			lastNode.gameObject.transform.position = transform.position;
		}

        UpdateLineRenderer();
	}

    protected override void OnCollision(Collision2D c)
    {
        if (!hasCollided)
            FinishRope(c);
		
        base.OnCollision(c);
    }

    void FinishRope(Collision2D c)
    {
		gameObject.GetComponent<Collider2D>().isTrigger = true;
        Rigidbody2D r = gameObject.GetComponent<Rigidbody2D>();
        r.constraints = RigidbodyConstraints2D.FreezeAll;
        
		target = c.gameObject.transform;
		targetOffset = transform.position - target.position;
        targetOffset.z = 0f;
        
		Node n = new Node(transform.position);

		n.rigid.constraints = RigidbodyConstraints2D.FreezeAll;
        n.joint.connectedBody = lastNode.rigid;
		if (c.gameObject.GetComponent<Rigidbody2D> () != null) {
			targetJoint = c.gameObject.AddComponent<SpringJoint2D> ();
			targetJoint.connectedBody = n.rigid;
			targetJoint.distance = 0.7f;
			targetJoint.frequency = 5f;
		}
		lastNode = n;
        nodes.Add(n);
    }

	void Stop() {
		gameObject.GetComponent<Rigidbody2D> ().velocity *= 0.2f;
		SpringJoint2D sp = gameObject.AddComponent<SpringJoint2D> ();
		sp.connectedBody = lastNode.rigid;
		sp.distance = 1f;
		sp.frequency = 10f;
	}

    void UpdateRope()
    {
        float dist = Vector2.Distance(new Vector2(transform.position.x, transform.position.y), new Vector2(lastNode.gameObject.transform.position.x, lastNode.gameObject.transform.position.y));
        if (dist > segmentLength) {
            Node n = new Node(transform.position);
            n.joint.connectedBody = lastNode.rigid;
            nodes.Add(n);
            lastNode = n;
			maxLength--;
        }
    }

    void UpdateLineRenderer()
    {
        rope.positionCount = hasCollided ? nodes.Count : nodes.Count + 1;
        rope.SetPosition(0, p.gameObject.transform.position);
        for (int i = 1; i < nodes.Count; i++)
            rope.SetPosition(i, nodes[i].gameObject.transform.position);
        if (!hasCollided)
            rope.SetPosition(nodes.Count, transform.position);
    }

    public override void AdditionalSetup(Player p)
    {
        this.p = p;
        rope = gameObject.AddComponent<LineRenderer>();
        rope.material = ropeMaterial;
        rope.textureMode = LineTextureMode.Tile;
        rope.sortingOrder = 10;
        rope.startWidth = 0.3f;
        rope.endWidth = rope.startWidth;
        nodes = new List<Node>();
        Node n = new Node(transform.position);
        n.joint.connectedBody = p.GetComponent<Rigidbody2D>();
        nodes.Add(n);
        lastNode = n;
    }

    private void OnDestroy()
    {
        foreach(Node n in nodes)
        {
			Destroy (n.gameObject);
        }
		if (targetJoint != null)
			Destroy (targetJoint);
    }

    private class Node
    {
        public GameObject gameObject;
        public Rigidbody2D rigid;
        public SpringJoint2D joint;
        public Collider2D c;

        public Node (Vector3 position)
        {
            gameObject = new GameObject("RopeNode");
            gameObject.transform.position = position;
            gameObject.layer = 8;
            rigid = gameObject.AddComponent<Rigidbody2D>();
            rigid.mass = 1f;
            joint = gameObject.AddComponent<SpringJoint2D>();
            joint.autoConfigureDistance = false;
            joint.distance = 0.7f;
			joint.frequency = 5f;
            c = gameObject.AddComponent<BoxCollider2D>();
        }
    }
}
