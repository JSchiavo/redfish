﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour {
    public float damage;
    public DamageType type;
    internal float scaledDamage;
    internal Vector3 direction;
}
