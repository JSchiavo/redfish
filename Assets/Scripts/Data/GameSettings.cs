﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameSettings {

    public static void IncreaseGraphicsQuality()
    {
        QualitySettings.IncreaseLevel();
    }

    public static void DecreaseGraphicsQuality()
    {
        QualitySettings.DecreaseLevel();
    }


}
