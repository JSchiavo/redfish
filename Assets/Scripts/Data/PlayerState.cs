﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;


public class PlayerState: PlayerElement {

    public int _lives = 1;
    public float _health = 100f;
    public int lives
    {
        get
        {
            return _lives;
        }
        set
        {
            _lives = value;
            if (_lives <= 0)
            {
                //GetPlayerEventManager().OnPlayerDeath();
            }
        }
    }
    public float health
    {
        get
        {
            return _health;
        }
        set
        {
            _health = value;
            if (_health <= 0)
            {
                GetPlayerEventManager().OnPlayerStunned();
                _health = 0f;
            }
            if (_health > 100f)
            {
                _health = 100f;
            }
        }
    }
	public Vector2 velocity;
	public Vector3 position;
	public float x;
	public float y;
	public float h;
	public float v;
	internal float rMag
    {
        get
        {
            return Mathf.Sqrt(Mathf.Pow(h, 2f) + Mathf.Pow(v, 2f));
        }
    }
    internal bool inWater = false;
    internal bool isStunned = false;
    internal bool canDblJump = true;
    internal bool isGrounded = false;
}
