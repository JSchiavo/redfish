﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class MatchManager : MonoBehaviour {

    public GameObject camera;

	public void LoadMatch(Character[] selections)
    {
        for (int i = 0; i < selections.Length; i++)
        {

            GameObject obj = (GameObject) GameObject.Instantiate(selections[i].characterPrefab);
            obj.transform.SetParent(transform);
            obj.transform.localPosition = Vector3.back;
            obj.GetComponent<Player>().ctrl = XCI.IntToController(i);
            camera.GetComponent<CameraController>().AddTarget(obj.transform.GetChild(0));
        }
    }
}
