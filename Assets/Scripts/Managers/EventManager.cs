﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;

/*
 * Global event manager. Designed to act like Unity's messaging system. Any GameElements that have methods that
 * match any of the names in any of the eventNames arrays will automatically be subscribed to that event.
 * For example if an array contains "OnPause" any GameElement with a method named OnPause will be subscribed to
 * the OnPause delegate.
 */
public class EventManager : MonoBehaviour {

	List<GameElement> gElements;

	//Game-Wide events 
	public delegate void GameEvent();
	//Global event with reference to the player who invoked the event
	public delegate void PlayerEvent(Player p);
	// Add name of new event to the array
	private string[] gameEventNames = new string[]{};
	private string[] playerEventNames = new string[]{"OnBasicShoot", "OnSpecialShoot", "OnGUISetup"};
	// Add public static events here to handle different events
	public static PlayerEvent OnBasicShoot;
	public static PlayerEvent OnSpecialShoot;
    public static PlayerEvent OnGUISetup;

	void Awake () {
		gElements = new List<GameElement> ();

		foreach (GameElement g in gameObject.GetComponentsInChildren<GameElement> ()) {
			gElements.Add (g);
			/* 
			 * Dynamically subscribe methods to delegates by searching each GameElement for a method with the
			 * same name.
			*/
			foreach (string methodName in gameEventNames)
				SubscribeMethodToDelegate<GameEvent>(g, methodName);
			foreach (string methodName in playerEventNames)
				SubscribeMethodToDelegate<PlayerEvent>(g, methodName);
		}
	}

	void SubscribeMethodToDelegate<T>(object target, string methodName) {
		FieldInfo info = this.GetType ().GetField (methodName);
		MethodInfo m = target.GetType ().GetMethod (methodName, BindingFlags.NonPublic | BindingFlags.Instance);
		if (m != null) {
			Delegate d = (Delegate)info.GetValue (this);
			d = Delegate.Combine(d, Delegate.CreateDelegate (typeof(T), target, m));
			info.SetValue (this, d);
		}
	}
}
