﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static GameManager manager;
	public PlayerSettings playerSettings;
    public MatchSettings matchSettings;
    private static Character[] selections;

	// Use this for initialization
	void Awake () {
		if (manager == null) {
			manager = this;
			playerSettings = GetComponent<PlayerSettings> ();
		}
        else if (manager != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
	}

    public void SaveCharacterSelections(Character[] selections)
    {
        GameManager.selections = selections;
    }

    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
		
		if (scene.name == "Main") {
			if (selections != null)
				GameObject.FindObjectOfType<MatchManager> ().LoadMatch (selections);
		}
    }
}
