﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using UnityEngine;

/*
 * Player specific event manager. Each player will have their own PlayerEventManager, that way thay can
 * have their PlayerElements react to events within the scope of specifically that player. Works same as
 * the global event manager in terms of subscribing to events.
 */
public class PlayerEventManager : MonoBehaviour {

	private List<PlayerElement> pElements;

	//Parameters for PlayerNotifcation are subject to change
	public delegate void PlayerNotification ();
	private string[] playerNotificationNames = new string[]{"OnGroundEnter",
		"OnGroundExit", "OnPlayerStunned", "OnPlayerRecovered"};
	public PlayerNotification OnGroundEnter;
	public PlayerNotification OnGroundExit;
    public PlayerNotification OnPlayerStunned;
    public PlayerNotification OnPlayerRecovered;
	// Add public PlayerNotifications here to handle different events

	void Awake() {
		pElements = new List<PlayerElement> ();

		foreach (PlayerElement p in gameObject.GetComponentsInChildren<PlayerElement> ()) {
			pElements.Add (p);
			/* 
			 * Dynamically subscribe methods to delegates by searching each PlayerElement for a method with the
			 * same name.
			*/
			foreach (string methodName in playerNotificationNames)
				SubscribeMethodToDelegate (p, methodName);
		}
	}

	void SubscribeMethodToDelegate(object target, string methodName) {
		FieldInfo info = this.GetType ().GetField (methodName);
		MethodInfo m = target.GetType ().GetMethod (methodName, BindingFlags.NonPublic | BindingFlags.Instance);
		if (m != null) {
			PlayerNotification d = (PlayerNotification)info.GetValue (this);
			d += (PlayerNotification)Delegate.CreateDelegate (typeof(PlayerNotification), target, m);
			info.SetValue (this, d);
		}
	}
}
