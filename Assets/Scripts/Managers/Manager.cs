﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : GameElement {

	public GameObject[] gameObjects;
	protected Dictionary<string, GameObject> store;

	void Awake() {
		store = new Dictionary<string, GameObject> ();
		foreach (GameObject g in gameObjects) {
			if (store.ContainsKey (g.name))
				Debug.Log (this.GetType () + " already contains a key for '" + g.name + "'");
			else
				store.Add (g.name, g);
		}
	}

	protected GameObject InstantiateWithKey(string key) {
		GameObject g = null;
		if (store.ContainsKey (key))
			g = (GameObject)Instantiate (store [key]);
		else
			Debug.Log (this.GetType () + ": No GameObject exists for key " + key);

		return g;
	}
}
