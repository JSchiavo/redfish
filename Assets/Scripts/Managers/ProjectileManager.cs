﻿using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : Manager
{

	public Projectile CreateProjectile(string key) {
		return InstantiateWithKey (key).GetComponent<Projectile>();
	}

    public void SetUpProjectile(Player p, Projectile projectile)
    {
		if (projectile != null) {
			projectile.transform.position = p.GetState ().position;
			projectile.transform.position = new Vector3 (projectile.transform.position.x, projectile.transform.position.y, -3f);
			//projectile.SetVelocity(new Vector3 (p.GetState ().h2 + Random.Range(0f, 0.25f), p.GetState ().v2  + Random.Range(0f, 0.25f), 0f) * projectile.intitialSpeed);
			Physics2D.IgnoreCollision (projectile.GetComponent<Collider2D> (), p.GetComponentInChildren<Collider2D> (), true);
		}
    }

	void OnBasicShoot(Player p) {
		//SetUpProjectile(p, CreateProjectile(p.GetState().basicProjectile));
	}

	void OnSpecialShoot(Player p) {
		//SetUpProjectile (p, CreateProjectile (p.GetState ().specialProjectile));
	}
}
