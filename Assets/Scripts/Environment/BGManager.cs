﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGManager : MonoBehaviour {

	public Color a;
	public Color b;
	public float speed = 1.0f;
	public Renderer sRenderer;

	// Use this for initialization
	void Start () {
		sRenderer = gameObject.GetComponent<Renderer> ();
		sRenderer.sharedMaterial = sRenderer.material;
	}
	
	// Update is called once per frame
	void Update () {
		float t = (Mathf.Sin (Time.time * speed) + 1.0f) / 2.0f;
		sRenderer.sharedMaterial.color = Color.Lerp (a, b, t);
	}
}
