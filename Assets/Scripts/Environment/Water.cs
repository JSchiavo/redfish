﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour {

	AreaEffector2D area;
    public float flushForce;
	public float flushInterval;
	public float flushDuration;
    private float currentForce;
	private float lastFlush;
	private float nextFlush;

	// Use this for initialization
	void Start () {
		area = gameObject.GetComponent<AreaEffector2D> ();
		lastFlush = Time.time;
		nextFlush = lastFlush + flushInterval;
        currentForce = -Physics2D.gravity.y;
    }
	
	// Update is called once per frame
	void Update () {
		if (Time.time > nextFlush) {
			Debug.Log ("FLUSH!");
			lastFlush = Time.time;
			nextFlush = lastFlush + flushInterval;
			StartCoroutine (Flush ());
		}
	}

	IEnumerator Flush() {
		float endFlush = Time.time + flushDuration;
        currentForce = flushForce;
		while (Time.time < endFlush) {
			yield return null;
		}
        currentForce = -Physics2D.gravity.y;
	}

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.attachedRigidbody != null)
            collision.attachedRigidbody.velocity += Vector2.up * currentForce * Time.fixedDeltaTime; 
    }
}
