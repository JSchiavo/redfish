﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeWaterTrigger : MonoBehaviour {

	public EdgeWaterRenderer parent;
	public int index;

	void OnTriggerEnter2D(Collider2D c) {
		if (c.attachedRigidbody != null) {
			/*if (parent.direction == EdgeWaterRenderer.Direction.Y) {
				parent.Splash (transform.position.x, c.attachedRigidbody.velocity.y * c.attachedRigidbody.mass / 40f);
			} else {
				parent.Splash (transform.position.y, c.attachedRigidbody.velocity.x * c.attachedRigidbody.mass / 40f);
			}*/
			parent.Splash (transform.position, c.attachedRigidbody.velocity * c.attachedRigidbody.mass / 400f, index);
		}
	}
}
