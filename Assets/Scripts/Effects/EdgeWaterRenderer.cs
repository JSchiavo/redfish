﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeWaterRenderer : MonoBehaviour {

	private LineRenderer edge;
	private GameObject edgeObject;
	public Material material;

	public Direction direction;
	public bool invert;

	private float width;
	private float height;
	public float waterHeight;
	private float waterLine;
	[Range(0.0f, 0.25f)]
	public float noise = 0.05f;

	private float[] xPos;
	private float[] yPos;
	private float[] xRad;
	private float[] yRad;
	private float[] xOrigin;
	private float[] yOrigin;

	private float[] velocity;
	private float[] acceleration;
	private float[] xVelocity;
	private float[] xAcceleration;
	private float[] yVelocity;
	private float[] yAcceleration;

	public int verticeCount = 10;

	private Mesh[] meshes;
	private GameObject[] meshObjects;
	public GameObject meshRenderer;
	private GameObject[] colliders;

	[Range(0.0f, 1.0f)]
	public const float springforce = 0.02f;
	[Range(0.0f, 1.0f)]
	public const float damping = 0.04f;
	[Range(0.0f, 0.5f)]
	public float spread = 0.05f;


	// Use this for initialization
	void Start () {
		xPos = new float[verticeCount];
		yPos = new float[verticeCount];
		xRad = new float[verticeCount];
		yRad = new float[verticeCount];
		xOrigin = new float[verticeCount];
		yOrigin = new float[verticeCount];

		velocity = new float[verticeCount];
		acceleration = new float[verticeCount];
		xVelocity = new float[verticeCount];
		xAcceleration = new float[verticeCount];
		yVelocity = new float[verticeCount];
		yAcceleration = new float[verticeCount];

		meshes = new Mesh[verticeCount - 1];
		meshObjects = new GameObject[verticeCount - 1];
		colliders = new GameObject[verticeCount];

		edgeObject = new GameObject ();
		edgeObject.transform.parent = transform;
		edge = edgeObject.AddComponent<LineRenderer> ();
		edge.useWorldSpace = true;
		edge.positionCount = verticeCount;
		edge.startWidth = 0.3f;
		edge.endWidth = 0.3f;
		edge.material = material;

		SetVertices ();
		CreateMeshes ();
		CreateColliders ();
	}

	void SetVertices() {
		Sprite sp = gameObject.GetComponent<SpriteRenderer> ().sprite;
		width = (2f * transform.localScale.x * sp.bounds.extents.x);
		height = (2f * transform.localScale.y * sp.bounds.extents.y);
		for (int i = 0; i < verticeCount; i++) {
			float x =  ((float)i / (float)(verticeCount - 1)) * Mathf.PI;
			float y = ((float)i / (float)(verticeCount - 1)) * Mathf.PI;

			xRad [i] = Mathf.Cos (x);
			yRad [i] = Mathf.Sin (y);
			xPos [i] = xRad [i] * (width / 2f) + transform.position.x;
			yPos [i] = yRad [i] * (height) + transform.position.y - height/ 2f;
			xOrigin [i] = xPos [i];
			yOrigin [i] = yPos [i];
			edge.SetPosition(i, new Vector3(xPos[i], yPos[i], -2f));
		}


		/*float dir = invert ? -1f : 1f;
		if (direction == Direction.Y) {
			waterLine = transform.position.y + dir * (height / 2f) - (dir * 0.5f);
			float xDelta = width / (float)(verticeCount - 1);
			for (int i = 0; i < verticeCount; i++) {
				xPos [i] = transform.position.x - (width / 2f) + i * xDelta;
				yPos [i] = waterLine;
				edge.SetPosition (i, new Vector3 (xPos [i], yPos [i], 0));
			}
		} else {
			waterLine = transform.position.x + dir * (width / 2f) - (dir * 0.5f);
			float yDelta = height / (float)(verticeCount - 1);
			for (int i = 0; i < verticeCount; i++) {
				xPos [i] = waterLine;
				yPos [i] = transform.position.y - (height / 2f) + i * yDelta;
				edge.SetPosition (i, new Vector3 (xPos [i], yPos [i], 0));
			}
		}*/
	}

	void CreateMeshes() {
		//float dir = invert ? -1f : 1f; 
		for (int i = 0; i < verticeCount - 1; i++) {
			meshes [i] = new Mesh ();

			Vector3[] vertices = new Vector3[4];

			vertices [0] = new Vector3 (xPos [i], yPos [i], -1f);
			vertices [1] = new Vector3 (xPos [i + 1], yPos [i + 1], -1f);
			vertices [2] = new Vector3 (xPos [i] + waterHeight * xRad[i], yPos [i] + waterHeight * yRad[i], -1f);
			vertices [2] = new Vector3 (xPos [i + 1] + waterHeight * xRad[i + 1], yPos [i + 1] + waterHeight * yRad[i + 1], -1f);
			/*
			if (direction == Direction.Y) {
				vertices [0] = new Vector3 (xPos [i], yPos [i], 0f);
				vertices [1] = new Vector3 (xPos [i + 1], yPos [i + 1], 0f);
				vertices [2] = new Vector3 (xPos [i], yPos [i] +  dir * waterHeight, 0f);
				vertices [3] = new Vector3 (xPos [i + 1], yPos [i + 1] +  dir * waterHeight, 0f);
			} else {
				vertices [0] = new Vector3 (xPos [i], yPos [i], 0f);
				vertices [1] = new Vector3 (xPos [i + 1], yPos [i + 1], 0f);
				vertices [2] = new Vector3 (xPos [i] + dir * waterHeight, yPos [i], 0f);
				vertices [3] = new Vector3 (xPos [i + 1] + dir * waterHeight, yPos [i + 1], 0f);
			}*/

			meshObjects [i] = GameObject.Instantiate<GameObject> (meshRenderer, Vector3.zero, Quaternion.identity);
			meshObjects [i].GetComponent<MeshRenderer> ().sharedMaterial = meshObjects [i].GetComponent<MeshRenderer> ().material;
			meshObjects [i].transform.parent = transform;
			meshObjects [i].GetComponent<MeshFilter> ().mesh = meshes [i];

			//TODO
			int[] triangles;
			/*if (!invert && direction == Direction.X) {
				triangles = new int[]{1,2,0,2,1,3};
			}
			else {
			 	triangles = new int[]{1,0,2,1,2,3};
			}*/
			triangles = new int[]{ 1, 2, 0, 2, 1, 3 };
			meshes [i].vertices = vertices;
			meshes [i].triangles = triangles;
			meshes [i].RecalculateNormals ();
		}
	}

	void CreateColliders() {
		for (int i = 0; i < verticeCount; i++) {
			colliders [i] = new GameObject ();
			colliders [i].layer = 4;
			colliders [i].name = "Trigger[" + i + "]";
			colliders [i].AddComponent<BoxCollider2D> ();
			colliders [i].AddComponent<EdgeWaterTrigger> ();
			colliders [i].GetComponent<EdgeWaterTrigger> ().parent = this;
			colliders [i].GetComponent<EdgeWaterTrigger> ().index = i;
			colliders [i].GetComponent<BoxCollider2D> ().isTrigger = true;
			colliders [i].transform.position = new Vector3 (xPos [i], yPos [i], 0f);
			colliders [i].transform.localScale = new Vector3 (width/verticeCount, width/verticeCount, 0f);
			colliders [i].transform.parent = edgeObject.transform;
		}
	}

	void UpdateMeshes() {
		//float dir = invert ? -1f : 1f; 
		for (int i = 0; i < meshes.Length; i++) {
			Vector3[] vertices = new Vector3[4];

			vertices [0] = new Vector3 (xPos [i], yPos [i], -1.5f);
			vertices [1] = new Vector3 (xPos [i + 1], yPos [i + 1], -1.5f);
			vertices [2] = new Vector3 (xOrigin [i] + waterHeight * xRad[i], yOrigin [i] + waterHeight * yRad[i], -1.5f);
			vertices [3] = new Vector3 (xOrigin [i + 1] + waterHeight * xRad[i + 1], yOrigin [i + 1] + waterHeight * yRad[i + 1],-1.5f);

			meshes [i].vertices = vertices;
		}
		/*
		for (int i = 0; i < meshes.Length; i++) {
			Vector3[] vertices = new Vector3[4];
			if (direction == Direction.Y) {
				vertices [0] = new Vector3 (xPos [i], yPos [i], -1f);
				vertices [1] = new Vector3 (xPos [i + 1], yPos [i + 1], -1f);
				vertices [2] = new Vector3 (xPos [i], yPos [i] + dir * waterHeight, -1f);
				vertices [3] = new Vector3 (xPos [i + 1], yPos [i + 1] + dir * waterHeight,-1f);
			} else {
				vertices [0] = new Vector3 (xPos [i], yPos [i], -1f);
				vertices [1] = new Vector3 (xPos [i + 1], yPos [i + 1], -1f);
				vertices [2] = new Vector3 (xPos [i] + dir * waterHeight, yPos [i], -1f);
				vertices [3] = new Vector3 (xPos [i + 1] + dir * waterHeight, yPos [i + 1], -1f);
			}

			meshes [i].vertices = vertices;
		}*/
	}

	void ApplySpringPhysics() {
		float[] xLeftDeltas = new float[xPos.Length];
		float[] xRightDeltas = new float[xPos.Length];
		float[] yLeftDeltas = new float[xPos.Length];
		float[] yRightDeltas = new float[xPos.Length];

		for (int i = 1; i < verticeCount - 1; i++) {
			float xForce = springforce * (xPos [i] - xOrigin[i]) + xVelocity [i] * damping;
			float yForce = springforce * (yPos [i] - yOrigin[i]) + yVelocity [i] * damping;
			xAcceleration [i] = -xForce;
			yAcceleration [i] = -yForce;
			xPos [i] += xVelocity [i];
			yPos [i] += yVelocity [i];
			xVelocity [i] += xAcceleration [i];
			yVelocity [i] += yAcceleration [i];
			edge.SetPosition (i, new Vector3 (xPos [i], yPos [i], -2f));
		}

		for (int j = 0; j < 8; j++) {
			for (int i = 0; i < xPos.Length; i++) {
				float xDistanceLeft = i > 0 ? xPos [i - 1] - xOrigin[i - 1] : 0f;
				float xDistanceRight = i < xPos.Length - 1 ? xPos [i + 1] - xOrigin[i + 1] : 0f;
				float xDistance = xPos [i] - xOrigin [i];
				float yDistanceLeft = i > 0 ? yPos [i - 1] - yOrigin[i - 1] : 0f;
				float yDistanceRight = i < yPos.Length - 1 ? yPos [i + 1] - yOrigin[i + 1] : 0f;
				float yDistance = yPos [i] - yOrigin [i];

				if (i > 0) {
					xLeftDeltas [i] = spread * (xDistance - xDistanceLeft);
					xVelocity [i - 1] += xLeftDeltas [i];
					yLeftDeltas [i] = spread * (yDistance - yDistanceLeft);
					yVelocity [i - 1] += yLeftDeltas [i];
				}
				if (i < yPos.Length - 1) {
					xRightDeltas [i] = spread * (xDistance - xDistanceRight);
					xVelocity [i + 1] += xRightDeltas [i];
					yRightDeltas [i] = spread * (yDistance - yDistanceRight);
					yVelocity [i + 1] += yRightDeltas [i];
				}
			}
		}

		for (int i = 0; i < xPos.Length; i++) {
			if (i > 0) {
				xPos [i - 1] += xLeftDeltas [i];
				yPos [i - 1] += yLeftDeltas [i];
			}
			if (i < xPos.Length - 1) {
				xPos [i + 1] += xRightDeltas [i];
				yPos [i + 1] += yRightDeltas [i];
			}
		}
	}

	public void Splash (Vector3 p, Vector3 vel, int index) {
		/*if (direction == Direction.Y) {
			if (p >= xPos [0] && p <= xPos [xPos.Length - 1]) {
				p -= xPos [0];
				int index = Mathf.RoundToInt ((xPos.Length - 1) * (p / width));
				velocity [index] = vel;
			}
		} else {
			if (p >= yPos [0] && p <= yPos [yPos.Length - 1]) {
				p -= yPos [0];
				int index = Mathf.RoundToInt ((yPos.Length - 1) * (p / height));
				velocity [index] = vel;
			}
		}*/
		int xIndex = 0;
		int yIndex = 0;
		if (p.x >= xPos [xPos.Length - 1] && p.x <= xPos [0]) {
			p.x -= xPos [xPos.Length - 1];
			xIndex = Mathf.RoundToInt ((xPos.Length - 1) * (p.x / width));
			xVelocity [index] = vel.x;
			yVelocity [index] = vel.y;
		}
		/*if (p.y >= yPos [0] && p.y <= yPos [(yPos.Length - 1)/2]) {
			p.y -= yPos [(yPos.Length - 1)/2];
			yIndex = Mathf.RoundToInt ((yPos.Length - 1) * (p.y / height));
			yIndex = xIndex > (xPos.Length - 1) / 2 ? (yPos.Length - 1) - yIndex : yIndex;
			yVelocity [yPos.Length - 1 - yIndex] = vel.y;
			Debug.Log ("y " + yIndex);
		}*/
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		/*for (int i = 0; i < verticeCount; i++) {
			if (direction == Direction.Y)
				yPos [i] += noise * Mathf.Sin ((Time.time * 5 + i));
			else
				xPos [i] += noise * Mathf.Sin ((Time.time * 5 + i));
					
		}*/
		ApplySpringPhysics ();
	}

	void Update() {
		UpdateMeshes ();
	}

	public enum Direction {
		X,
		Y
	}
}
