﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteController))]
public class SpriteControllerEditor : Editor {

	SpriteController sc;

	void Awake() {
		sc = target as SpriteController;
	}


    public override void OnInspectorGUI()
    {
        sc.Head = (GameObject)EditorGUILayout.ObjectField("Head", sc.Head, typeof(GameObject), true);
        sc.useIK = GUILayout.Toggle(sc.useIK, "Use IK");

        if (sc.useIK)
        {
            sc.Arm = null;
            sc.Bone1 = (GameObject) EditorGUILayout.ObjectField("Bone1", sc.Bone1, typeof(GameObject), true);
            sc.Bone2 = (GameObject) EditorGUILayout.ObjectField("Bone2", sc.Bone2, typeof(GameObject), true);
            
        }
        else
        {
            sc.Bone1 = null;
            sc.Bone2 = null;
            sc.Arm = (GameObject)EditorGUILayout.ObjectField("Arm", sc.Arm, typeof(GameObject), true);
        }
		sc.speed = EditorGUILayout.FloatField("Speed", sc.speed);
        sc.armOffset = EditorGUILayout.FloatField("Arm Offset", sc.armOffset);
        sc.min = EditorGUILayout.FloatField("Min", sc.min);
        sc.max = EditorGUILayout.FloatField("Max", sc.max);
    }
}
