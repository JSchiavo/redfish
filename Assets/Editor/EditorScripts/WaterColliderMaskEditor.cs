﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WaterColliderMask))]
public class WaterColliderMaskEditor : Editor {

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		WaterColliderMask s = (WaterColliderMask)target;
		if (GUILayout.Button ("Build Water Colliders"))
			s.Execute ();
	}

}
